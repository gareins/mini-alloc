#ifndef ALLOC_H
#define ALLOC_H

#include <stddef.h>

void oreset();
void* omalloc(size_t size);
void ofree(void* ptr);
void* orealloc(void* ptr, size_t new_size);

float pool_taken();

#endif
