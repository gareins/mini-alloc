#include "alloc.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define MALLOC omalloc
#define REALLOC orealloc
#define FREE ofree

#define ARRAY_SIZE 1024
#define RESET 20000
#define ALLOC_SIZE 512

int main() {
	time_t t;
	srand((unsigned) time(&t));
	unsigned bad_allocs = 0;
	unsigned good_allocs = 0;
	unsigned ops = 0;

	void* pointers[ARRAY_SIZE] = { 0 };

	for(unsigned i = 0; 1; i++) {
		if(rand() % ARRAY_SIZE != 0) {
			continue;
		}

		ops += 1;
		if(ops % RESET == 0) {
			memset(pointers, 0, sizeof(pointers));
			oreset();
		}

		unsigned idx = i % ARRAY_SIZE;
		if(pointers[idx] == NULL) {
			pointers[idx] = MALLOC(rand() % ALLOC_SIZE);
			bad_allocs += pointers[idx] == NULL;
			good_allocs += pointers[idx] != NULL;
		}
		else {
			if(rand() % 5 == 0) {
				pointers[idx] = REALLOC(pointers[idx], rand() % ALLOC_SIZE);
				bad_allocs += pointers[idx] == NULL;
				good_allocs += pointers[idx] != NULL;
			}
			else {
				FREE(pointers[idx]);
				pointers[idx] = NULL;
			}
		}

		unsigned f = 0;
		for(unsigned j = 0; j < ARRAY_SIZE; j++) {
			f += pointers[j] == NULL ? 1 : 0;
		}

		printf("\r%09d free %d/%d bad/good %09d/%09d full%.3f", ops, f, ARRAY_SIZE, bad_allocs, good_allocs, pool_taken());
		fflush(stdout);
	}

	printf("\n");
	return 0;
}
