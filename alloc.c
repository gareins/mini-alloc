#include "alloc.h"
#include <stdlib.h>

#include <stdio.h>
#include <assert.h>

#ifndef POOL_SIZE
#define POOL_SIZE ((int)(1024 * 256 * 3 / 4))
#endif

typedef struct allocation_info_node allocation_info_node_t;

struct allocation_info_node {
	size_t size; // size without node size
	allocation_info_node_t* next;
};

char allocation_pool[POOL_SIZE];

#define ALLIGNMENT sizeof(void*)

#define NODE_SIZE sizeof(allocation_info_node_t)
#define ALLIGN(x) (((x) + ALLIGNMENT - 1) / ALLIGNMENT) * ALLIGNMENT

#define HEAD() ((allocation_info_node_t*)allocation_pool)
#define END ((allocation_info_node_t*)(&allocation_pool[POOL_SIZE]))

#define NODE_OFFSET(node, offset) (allocation_info_node_t*)(((char*)(node)) + (offset))

void oreset() {
	allocation_info_node_t* head = HEAD();
	head->size = 0; 
	head->next = NULL;
}

void* omalloc(size_t size) {
	allocation_info_node_t* prev = HEAD();
	allocation_info_node_t* node = prev->next;
	size_t size_plus = size + NODE_SIZE;

	while(node != NULL) {
		size_t hole = (char*)node - (char*)prev - NODE_SIZE - prev->size;
		if(hole >= size_plus) { // simple first match
			allocation_info_node_t* inner = NODE_OFFSET(prev, prev->size + NODE_SIZE);
			inner->size = size;
			inner->next = node;
			prev->next = inner;
			return NODE_OFFSET(inner, NODE_SIZE);
		}

		prev = node;
		assert(node != node->next);
		node = node->next;
	}

	// go one back to last valid
	node = prev;

	// no hole to fill, try to append to the end
	allocation_info_node_t* tail = NODE_OFFSET(node, ALLIGN(node->size + NODE_SIZE));

	size_t available_space = (char*)END - (char*)tail;
	if(END > tail && available_space >= size_plus) {
		tail->size = size;
		tail->next = NULL;
		node->next = tail;
		return NODE_OFFSET(tail, NODE_SIZE);
	}

	return NULL;
}

void ofree(void* ptr) {
	if(ptr == NULL) {
		return;
	}
		
	allocation_info_node_t* prev = HEAD();
	allocation_info_node_t* node = HEAD();

	while(node != NULL) {
		if(NODE_OFFSET(node, NODE_SIZE) == ptr) {
			prev->next = node->next;
			return;
		}

		prev = node;
		assert(node != node->next);
		node = node->next;
	}

	// trying to free non allocated :(
	exit(1);
}

void* orealloc(void* ptr, size_t new_size) {
	if(ptr == NULL) {
		return omalloc(new_size);
	}

	allocation_info_node_t* node = HEAD();
	allocation_info_node_t* realloc_node = NULL;

	while(node != NULL) {
		if(NODE_OFFSET(node, NODE_SIZE) == ptr) {
			realloc_node = node;
			break;
		}
		assert(node != node->next);
		node = node->next;
	}

	if(realloc_node == NULL) { // failed to find :(
		exit(2);
	}
	else if(realloc_node->size >= new_size) {
		realloc_node->size = new_size;
	}
	else {
		void* new_ptr = omalloc(new_size);
		if(new_ptr == NULL) {
			return NULL;
		}

		char* ptr8 = ptr;
		char* new_ptr8 = new_ptr;
		for(unsigned i = 0; i < new_size; i++) {
			new_ptr8[i] = ptr8[i];
		}

		ofree(ptr);
		ptr = new_ptr;
	}

	return ptr;
}

void* ocalloc(size_t num, size_t bucket) {
	size_t size = num * bucket;
	void* ptr = omalloc(size);
	if(ptr == NULL) {
		return NULL;
	}

	char* ptr8 = ptr;
	for(unsigned i = 0; i < size; i++) {
		ptr8[i] = 0;
	}

	return ptr;
}

float pool_taken() {
	size_t result = 0;

	allocation_info_node_t* node = HEAD();
	while(node != NULL) {
		result += node->size;

		assert(node != node->next);
		node = node->next;
	}

	return 1.0 * result / POOL_SIZE;
}

